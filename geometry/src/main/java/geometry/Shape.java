package geometry;

public interface Shape {

       void getArea();
       void getPerimeter();
    
} 
